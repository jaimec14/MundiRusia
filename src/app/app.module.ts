import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { TabsPage } from '../pages/tabs/tabs';
import { AboutPage } from '../pages/about/about';
import { PartidoPage } from '../pages/partido/partido';


import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { KeysPipe } from '../pipes/pipe.keys';

import { HttpModule } from '@angular/http';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireModule } from 'angularfire2';
//import { FirebaseServiceProvider } from '../providers/firebase-service/firebase-service';

export const firebaseConfig = {
  apiKey: "AIzaSyBhly9_WYluVdpIjFnE5ggyq4al-poDkPA",
  authDomain: "ionicfbdb-80d63.firebaseapp.com",
  databaseURL: "https://ionicfbdb-80d63.firebaseio.com",
  projectId: "ionicfbdb-80d63",
  storageBucket: "ionicfbdb-80d63.appspot.com",
  messagingSenderId: "822177626471"
 };

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    ListPage,
    TabsPage,
    AboutPage,
    PartidoPage,
    KeysPipe
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AngularFireDatabaseModule,
    AngularFireModule.initializeApp(firebaseConfig),
    IonicModule.forRoot(MyApp),
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    ListPage,
    AboutPage,
    PartidoPage,

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    //FirebaseServiceProvider
  ]
})
export class AppModule {}
