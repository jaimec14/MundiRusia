import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireList } from 'angularfire2/database';
import { DomSanitizer } from '@angular/platform-browser';
import { PartidoPage } from '../partido/partido';
/**
 * Generated class for the AboutPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {
  pais:any;
  jugadores:any;
  fondo:string;
  partidos:any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private sanitizer: DomSanitizer,public db: AngularFireDatabase) {
    this.pais = navParams.get('data');
    console.log(this.pais);
    this.jugadores = this.pais.Jugadores;
    console.log(this.jugadores);
    this.partidos = this.pais.Partidos;
    console.log(typeof this.jugadores.Axel_Witsel);
  //  this.fondo = this.sanitizer.bypassSecurityTrustStyle(`url(${this.pais.Bandera})`);

  }



  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');

  }

  verPartido(partido){
    console.log("ver partido");
    this.navCtrl.push(PartidoPage,{
      data:partido
    });
  }
}
