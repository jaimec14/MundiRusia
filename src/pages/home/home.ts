import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AngularFireList } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireDatabase } from 'angularfire2/database';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
paises: Observable<any[]>
  constructor(public navCtrl: NavController, db: AngularFireDatabase) {
    this.paises = db.list('Paises').valueChanges();

  }

}
