import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs/Observable';
import { AngularFireList } from 'angularfire2/database';
import { AboutPage } from '../about/about';
@Component({
  selector: 'page-list',
  templateUrl: 'list.html'
})
export class ListPage {
  pais: AngularFireList<any>
  paises: Observable<any[]>

  constructor(public navCtrl: NavController, public navParams: NavParams, public db: AngularFireDatabase) {
    // If we navigated to this page, we will have an item available as a nav param
    this.paises = db.list('Paises').valueChanges();
  }

  cambiarPagina(pais){
    this.navCtrl.push(AboutPage,{
      data: pais
    });
  }

}
