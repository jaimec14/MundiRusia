import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the PartidoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-partido',
  templateUrl: 'partido.html',
})
export class PartidoPage {
partidos:any;
fecha:any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.partidos = navParams.get('data');
    console.log(this.partidos);
    this.fecha = this.partidos.value.Fecha;

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PartidoPage');
  }

}
